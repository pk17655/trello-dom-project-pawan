let keys = {
  key: "4922d8cb43c158cd71bd4042c7ae3cf5",
  token: "62f7810750c045a21c8117b6da1cae192448c23eb470c00cddaf99ec5fb0279d",
  boardId: "tmy39wPP"
};

async function fetchChecklist() {
  let checklist = await fetch(
    `https://api.trello.com/1/boards/${keys.boardId}/checklists?token=${keys.token}&key=${keys.key}`
  );
  let checklistJson = await checklist.json();
  return checklistJson;
}

let cardId;
let checkListId;
async function checklistItemArray() {
  let checkListItem = [];
  let checklist = await fetchChecklist();
  checklist.forEach(element => {
    cardId = element.idCard;
    checkListId = element.id;

    element.checkItems.forEach(subElement => {
      checkListItem.push(subElement);
    });
  });
  return checkListItem;
}

/*--------------DISPLAY CHECKLIST ITEM--------------*/

function displayChecklistItems(checkItem) {
  let li = document.querySelector("li");
  let liClone = li.cloneNode(true);
  liClone.querySelector("p").textContent = checkItem.name;

  if (checkItem.state == "complete") {
    liClone.querySelector("p").style.textDecoration = "line-through";
    liClone.querySelector("#check").setAttribute("checked", "true");
  }

  liClone.querySelector("p").setAttribute("itemId", checkItem.id);
  liClone.querySelector("p").setAttribute("checklistId", checkItem.idChecklist);
  liClone.querySelector("p").setAttribute("cardId", cardId);

  liClone.style.display = "flex";
  document.querySelector("ul").appendChild(liClone);
}

/*-----------ADDING NEW CHECKLIST ITEMS-----------*/

async function addNewChecklistItemEvent() {
  let inputItem = document.querySelector(".input-item");

  inputItem.addEventListener("submit", function(e) {
    e.preventDefault();

    itemName = inputItem.querySelector("input[type='text']").value;
    if (itemName == "") {
      alert("please enter something");
    } else {
      inputItem.querySelector("input[type='text']").value = "";
      postNewChecklistItem(itemName);
    }
  });
}

/*--------------POST CHECKITEM METHOD-------------*/

async function postNewChecklistItem(itemName) {
  await fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${itemName}&key=${keys.key}&token=${keys.token}`,
    {
      method: "POST"
    }
  );

  let checklistItem = await checklistItemArray();
  displayChecklistItems(checklistItem[checklistItem.length - 1]);
}

/*--------------DELETING CHECKLIST ITEMS-------------*/

function deleteChecklistItemEvent() {
  let checklistItems = document.querySelector("ul");

  checklistItems.addEventListener("click", function(e) {
    if (e.target.id == "delete") {
      let checklistItem = e.target.parentElement;
      checklistID = checklistItem
        .querySelector("p")
        .getAttribute("checklistId");
      checkItemID = checklistItem.querySelector("p").getAttribute("itemId");
      deleteChecklistItem(checklistID, checkItemID);
      checklistItems.removeChild(checklistItem);

      deleteChecklistItem(checklistID, checkItemID);
    }
  });
}

/*------------------------DELETE CHECKITEM METHOD----------------------*/

async function deleteChecklistItem(checklistID, checkItemID) {
  await fetch(
    `https://api.trello.com/1/checklists/${checklistID}/checkItems/${checkItemID}?key=${keys.key}&token=${keys.token}`,
    {
      method: "DELETE"
    }
  );
}

/*-----------------UPDATE STATE OF CHECKLIST ITEMS---------------*/

function updateChecklistItemStateEvent() {
  let checklistItems = document.querySelector("ul");
  checklistItems.addEventListener("click", function(e) {
    if (e.target.id == "check") {
      let checkItem = e.target.parentElement;
      let cardId = checkItem.querySelector("p").getAttribute("cardId");
      let itemId = checkItem.querySelector("p").getAttribute("itemId");

      let state = "incomplete";

      if (e.target.checked == true) {
        state = "complete";
        checkItem.querySelector("p").style.textDecoration = "line-through";
      } else {
        checkItem.querySelector("p").style.textDecoration = "";
      }

      updateCheckItemState(cardId, itemId, state);
    }
  });
}

/*-----------------PUT CHECKITEM STATE METHOD------------------*/

async function updateCheckItemState(cardId, checkItemId, state) {
  await fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${state}&key=${keys.key}&token=${keys.token}`,
    {
      method: "PUT"
    }
  );
}

/*---------------------UPDATE NAME OF CHECKLIST ITEMS-----------*/

function updateChecklistItemNameEvent() {
  let checkItems = document.querySelector("ul");

  checkItems.addEventListener("click", function(e) {
    if (e.target.id == "text") {
      let checkItem = e.target.parentElement;
      let cardId = checkItem.querySelector("p").getAttribute("cardId");
      let itemId = checkItem.querySelector("p").getAttribute("itemId");
      let itemName;

      e.target.addEventListener("input", function() {
        itemName = e.target.textContent;

        e.target.addEventListener("keypress", function(e) {
          if (e.keyCode == 13) {
            e.preventDefault();

            updateCheckItemName(cardId, itemId, itemName);
          }
        });
      });
    }
  });
}

/*------------------PUT CHECKITEM NAME METHOD-----------------------*/

async function updateCheckItemName(cardId, checkItemId, name) {
  await fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?name=${name}&key=${keys.key}&token=${keys.token}`,
    {
      method: "PUT"
    }
  );
}

/*--------------------HELPER FUNCTION FOR CALLING OTHER FUNCTION---------------- */

async function helperFunction() {
  let checkListItems = await checklistItemArray();
  checkListItems.forEach(checkItem => {
    displayChecklistItems(checkItem);
  });
  addNewChecklistItemEvent();
  deleteChecklistItemEvent();
  updateChecklistItemStateEvent();
  updateChecklistItemNameEvent();
}
helperFunction();
